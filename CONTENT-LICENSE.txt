## Wolfire assets (CC-BY-SA 3.0 Unported)

All assets from the original Lugaru game (including Lugaru HD assets)
are distributed under the Creative Commons Attribution-Share Alike license
in version 3.0 Unported (CC-BY-SA 3.0 Unported):
http://creativecommons.org/licenses/by-sa/3.0/

Attribution to Wolfire Games: http://www.wolfire.com

Those assets are all the files in the `Data` folder, except from the
ones listed below with different licenses or credits.

## Slib assets (CC-BY-SA)

http://forums.wolfire.com/viewtopic.php?f=7&t=1066&start=270#p109473

These assets are from the Temple campaign by Slib under CC-BY-SA:

Data/Campaign/temple.txt

Data/Maps/sven*

Data/Textures/Desertcl.png
Data/Textures/GiTop.png
Data/Textures/GiTeared.png
Data/Textures/GiBottom.png
Data/Textures/mask.png
Data/Textures/temple/World.png

## Wolfire nonfree but redistributable assets

All other game assets and demo data (should all be in "Data" folder
in the root of the source tree) are not under the same license.
Wolfire has allowed the other data to be freely redistributed
for non commercial purposes, but it is forbidden to use in any revenue
generating works.
